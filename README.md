

**Author:** Kuan-Hua (Françoise) Artignan  
**Tutor:** Laurent Gueguen

# Table of Content
- [Table of Content](#table-of-content)
- [Introduction](#introduction)
- [DGINN-Snakemake pipeline](#dginn-snakemake-pipeline)
  - [Installation - The manual way](#installation---the-manual-way)
    - [Dependencies :](#dependencies-)
      - [Singularity](#singularity)
      - [Snakemake](#snakemake)
      - [Java](#java)
    - [Dependecies to run the pipeline without Singularity :](#dependecies-to-run-the-pipeline-without-singularity-)
    - [STEP 1 :](#step-1-)
    - [STEP 2 :](#step-2-)
    - [STEP 3 :](#step-3-)
  - [Installation - The Docker way](#installation---the-docker-way)
- [DGINN-Snakemake-API](#dginn-snakemake-api)
  - [Installation - The manual way](#installation---the-manual-way-1)
    - [STEP 1 :](#step-1--1)
    - [STEP 2 :](#step-2--1)
  - [Installation - The Docker way](#installation---the-docker-way-1)
  - [Questions and Suggestions](#questions-and-suggestions)

# Introduction
Welcome to DGINN-API, my internship work consisting of two parts: DGINN-Snakemake and DGINN-API. This project focuses on the detection of genetic innovations using nucleotide sequences. The pipeline automates various preliminary steps for evolutionary analyses, such as retrieving homologs, assigning them to orthology groups, codon alignment, and reconstructing gene phylogeny.

# DGINN-Snakemake pipeline

Ideally, we aim to leverage Docker deployment to streamline the work of researchers, providing them with a seamless experience. By installing Docker and running it as a server, they can effortlessly access the API. In case Docker encounters any issues, an alternative approach is to install Python Flask and the necessary dependencies on your local computer. This allows you to run the server locally, ensuring uninterrupted access to the API.

## Installation - The manual way
To install the program, please follow these steps

Download the DGINN-API repository.
   ```
   git clone https://gitlab.com/kuanhua.artignan/dginn-snakemake-api.git
   cd dginn-snakemake-api

   ```

The GitLab repository includes two parts:

- Workflows: DGINN, RNAseq (example), DNAseq (example)
- API: Server (app.py and server.py), Templates and Statics directories.
  
```
├── DGINN(principle workflow)
│   ├── config
│   ├── data
│      ├── ex_CCDS.fasta
│      ├── ex_aln.fasta
│      ├── ex_geneTree.tree
│      ├── ex_spTree.tree
│      ├── species_list
│   ├── lib
│   ├── rules
│   ├── scripts
│   ├── Snakefile
├── DNAseq(example)
├── RNAseq(example)
│
├── statics
├── templates
├── app.py
├── server.py
```

### Dependencies :

#### Docker

```sh
sudo snap install docker
```

#### Singularity
To use the Snakemake version of the pipeline, you will first need to install singularity.

To do so, follow [this tutorial](https://github.com/sylabs/singularity/blob/main/INSTALL.md)

***Important, with some reasons, user can use the older version of singularity and go  

```sh
sudo apt-get update && \
sudo apt-get install -y \
    build-essential \
    libseccomp-dev \
    libglib2.0-dev \
    pkg-config \
    squashfs-tools \
    cryptsetup \
    crun \
    uidmap \
    git \
    wget

sudo rm -r /usr/local/go

export VERSION=1.20.5 OS=linux ARCH=amd64  # change this as you need

wget -O /tmp/go${VERSION}.${OS}-${ARCH}.tar.gz https://dl.google.com/go/go${VERSION}.${OS}-${ARCH}.tar.gz && \
    sudo tar -C /usr/local -xzf /tmp/go${VERSION}.${OS}-${ARCH}.tar.gz

echo 'export GOPATH=${HOME}/go' >> ~/.bashrc && \
echo 'export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc && \
source ~/.bashrc

curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

mkdir -p ${GOPATH}/src/github.com/sylabs && \
    cd ${GOPATH}/src/github.com/sylabs && \
    git clone --recurse-submodules https://github.com/sylabs/singularity.git

cd singularity

git checkout v3.11.4

./mconfig
cd ./builddir
make
sudo make install

singularity version
```

#### Snakemake and Conda
To install Snakemake, you will first need to install conda. [Here is the guide for that.](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)
The minimum version of snakemake is "6.15.1".
The snakemake installation is detailed [here](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html)

Rapide installation
```sh
sudo apt install snakemake
```

#### Java
The use of the MACSE codon aligner tool with this pipeline requires that you have a java sdk installed.

To install one, type this command :

```sh
sudo apt install default-sdk
```

### Dependecies to run the pipeline without Singularity :

To start the Snakemake DGINN pipeline from the command line, without Singulairy, you first need to install all the pipeline's dependencies : 
- [EMBOSS:6.6](http://en.bio-soft.net/format/emboss.html), 
- [PhyML 3.0](https://github.com/stephaneguindon/phyml), 
- [PRANK v.170427](http://wasabiapp.org/software/prank/prank_installation/), 
- [Treerecs v1.0](https://gitlab.inria.fr/Phylophile/Treerecs), 
- [HYPHY 2.3](http://www.hyphy.org/installation/)

To install Snakemake's python dependencies, first [install conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)

***Important, if singularity and docker can't support some dependencies, we can use the rapid installation
```sh
sudo apt-get install -y mafft
sudo apt-get -y install phyml
```

### STEP 1 : 

Create and activate conda environment:

```sh
conda create -n snakemake python
conda activate snakemake
```
This command will install all the python dependencies of Snakemake and activate it.
To install Snakemake via pip in a conda environment, use this command:

```sh
pip install git+https://github.com/snakemake/snakemake
```

### STEP 2 :

Create configfile in format json for the pipeline configurations

```sh
cd DGINN/
pip install -r requirements.txt

python configObj_DGINN.py data/ex_CCDS.fasta data/ex_geneTree.tree data/ex_spTree.tree config/configfile.json
```

### STEP 3 :

Run the pipeline DGINN-Snakemake with command:

```sh
snakemake -c20 -p --use-singularity --latency-wait 1000
```
Users can replace '--cores 1' or '-c1' with '-cx', where 'x' represents the number of cores they want the pipeline to use.

If the singularity has the problem, we can install all dependancies and run the command :
```sh
snakemake -c20 -p --latency-wait 1000
```

## Installation - The Docker way

```bash


```

# DGINN-Snakemake-API

Python Flask is a micro web framework that allows you to build web applications and APIs in Python. It is designed to be simple, lightweight, and easy to use, making it a popular choice for developing web applications.

```
├── statics
│   ├── css
│   ├── img
│   ├── js
├── templates
│   ├── controler.html
│   ├── DGINN.html
│   ├── viewTree.html
├── app.py
├── server.py
```

## Installation - The manual way
To install the Flask environment and set up a development environment for Flask, user has to execute this command first.

### STEP 1 : 
Use the command below to install the packages according to the configuration file requirements.txt.

```sh
pip install -r requirements.txt
```
### STEP 2 : 
Run server in the terminal using the python command:

```sh
python server.py
```

## Installation - The Docker way

```bash


```
## Questions and Suggestions
If you have any questions or suggestions regarding the DGINN-API program, please feel free to contact us at the following email addresses:

- lea.picard@ens-lyon.fr
- laurent.gueguen@univ-lyon1.fr
- lucie.etienne@ens-lyon.fr
- kuan-hua.tu@etu.univ-lyon1.fr

We appreciate your feedback and are here to assist you with any queries you may have.

