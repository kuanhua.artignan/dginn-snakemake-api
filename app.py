from flask import Flask, render_template, request, jsonify, Response, redirect, url_for, abort, send_file
import configObj_DGINN
from werkzeug.utils import secure_filename
from subprocess import Popen, PIPE
from subprocess import check_output
import re, os, shutil, logging, json
from pathlib import Path
from DGINN.lib.genere_speciestree_xml import createPhyloXML
from DGINN.lib.initConfig import paramDef, initLogger
from Bio import SeqIO, Phylo
import time, datetime
from io import BytesIO

app = Flask(__name__, template_folder="./templates", static_folder="./statics")

@app.route('/')
def home():
    return render_template('DGINN.html')

PIPELINE_ROOT = os.path.dirname(os.path.abspath(__file__))

@app.route('/select_workflow', methods=['POST'])
def select_workflow():
    selected_workflow = request.form['folder']
    # do something with the selected folder
    return redirect(url_for('folder', workflow=selected_workflow))

@app.route('/<workflow>')
def folder(workflow):
    if workflow not in ['DGINN', 'RNAseq', 'DNAseq']:
        workflow = 'DGINN'
    # do something with the selected folder
    template_name = f'{workflow}.html'
    if template_name not in ['DGINN.html', 'RNAseq.html', 'DNAseq.html']:
        template_name = 'DGINN.html'
    return render_template(template_name, folder_name=workflow)

ALLOWED_EXTENSIONS = {'fasta', 'tree'}

def allowed_file(filename, extension):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in extension

def create_dir_if_not_exists(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def save_workspace(workflow):
    path_workflow = os.path.join(app.instance_path, workflow)
    if os.path.exists(path_workflow):
        now = datetime.datetime.now()
        os.rename(path_workflow,path_workflow+"_"+now.strftime("%Y-%m-%d %H:%M:%S"))

def generate_workspace(workflow):
    path_workflow = os.path.join(app.instance_path, workflow)
    if os.path.exists(path_workflow):
        now = datetime.datetime.now()
        os.rename(path_workflow,path_workflow+"_"+now.strftime("%Y-%m-%d %H:%M:%S"))

    create_dir_if_not_exists(path_workflow+'/config')
    create_dir_if_not_exists(path_workflow+'/data')
    create_dir_if_not_exists(path_workflow+'/results')
    shutil.copy("./DGINN/config/config.schema.yaml",path_workflow+'/config/config.schema.yaml')
    shutil.copytree("./DGINN/lib",path_workflow+'/lib')
    shutil.copytree("./DGINN/rules",path_workflow+'/rules')
    shutil.copytree("./DGINN/scripts",path_workflow+'/scripts')
    shutil.copy("./DGINN/data/species_list",path_workflow+'/results/species_list')
    shutil.copy("./DGINN/macse_v2.06.jar",path_workflow+'/macse_v2.06.jar')
    shutil.copy("./DGINN/Snakefile",path_workflow+'/Snakefile')

@app.route('/upload_dginn', methods=['POST'])
def upload_dginn():
    workflow = "DGINN"

    ######## request from the client
    fasta_file = request.files.get('fasta_file')
    tree_file = request.files.get('tree_file')
    sptree_file = request.files.get('sptree_file')
    duplication = request.form.get('duplication')=='true'
    recombination = request.form.get('recombination')=='true'
    positive_selection = request.form.get('positiveSelection')=='true'
    api_key = request.form.get('api-key')

    # align_codon = []
    # if request.form.get('prank')=='true':
    #     align_codon.append('prank')
    # if request.form.get('macse')=='true':
    #     align_codon.append('macse') 
    # align_nt = request.form.get('nt')=='true' 
    
    align_codon = ''
    if request.form.get('prank') == 'true':
        align_codon += 'prank'
    if request.form.get('macse') == 'true':
        align_codon += 'macse'
    align_nt = request.form.get('nt') == 'true'

    # verefy the format of the request input
    if fasta_file and not allowed_file(fasta_file.filename, ["fasta"] ):
        abort(400, 'File format for FASTA file is invalid.')
    if tree_file and not allowed_file(tree_file.filename, ["tree"] ):
        abort(400, 'The file format for the tree file is invalid.')
    if sptree_file and not allowed_file(sptree_file.filename, ["fasta"] ):
        abort(400, 'The file format for the tree file is invalid.')

    generate_workspace(workflow)
    
    fasta_filename, fasta_path = saveFile(fasta_file,workflow)
    tree_filename, tree_path = saveFile(tree_file,workflow)
    sptree_filename, sptree_path = saveFile(sptree_file,workflow)
    
    configfile_obj, path_config_file = setConfigFile_DGINN(workflow, "configfile.json", fasta_path, tree_path, sptree_path)
    configfile_obj.is_duplication(duplication)
    configfile_obj.is_recombination(recombination)
    configfile_obj.is_positiveSelection(positive_selection)
    configfile_obj.setAlignCodon(align_codon)
    configfile_obj.setAlignNT(align_nt)
    configfile_obj.setAPIKey(api_key)

    outdir = configfile_obj.getOutDirPath()

    objectJson = configfile_obj.writeObjects()
    open(path_config_file, 'w').write(objectJson)

    if  len(list(SeqIO.parse(configfile_obj.getInputFile(),'fasta'))) > 1:
        shutil.copy(fasta_path, outdir+'align_'+fasta_filename+'.fasta')
        shutil.copy(tree_path, outdir+tree_filename+'.tree')
    else:
        shutil.copy(fasta_path, outdir+'blast_input_'+fasta_filename+'.fasta')
        shutil.copy(tree_path, outdir+tree_filename+'.tree')

    run_DGINN()
    save_workspace(workflow)
    return jsonify({'status':'success'})

def saveFile(file,workflow):
    filename = secure_filename(file.filename).split(".")[0]
    path = os.path.join(app.instance_path, workflow+"/data", filename)
    file.save(path)
    return filename,path

def setConfigFile_DGINN(workflow, configfilename, path_input_fastafile, path_input_treefile, path_input_sptree):
    
    path_workflow = os.path.join(app.instance_path, workflow)
    secure_config_filename = secure_filename(configfilename)
    path_configfile = os.path.join(app.instance_path, workflow,"config", secure_config_filename)
    if not os.path.exists(path_workflow+'/config'):
        os.makedirs(path_workflow+'/config')

    configfile_obj = configObj_DGINN.Configuration()
    if len(list(SeqIO.parse(path_input_fastafile,'fasta'))) > 1:
        configfile_obj.setStep("option")
        configfile_obj.setAlnFile(path_input_fastafile)
    else:
        configfile_obj.setStep("blast")
        configfile_obj.setInputFile(path_input_fastafile)
        
    configfile_obj.setTreeFile(path_input_treefile)
    configfile_obj.setSpTreeFile(path_input_sptree)

    configfile_obj.setAPIKey("d636d39ffc08e83d78812daac80693ef9108")

    if not os.path.exists(path_workflow+'/results/'):
        os.makedirs(path_workflow+'/results/')
        configfile_obj.setOutDir(path_workflow+'/results/')
    configfile_obj.setOutDir(path_workflow+'/results/')
   
    version="1.0"
    parameters_complete = paramDef(configfile_obj.properties["parameters"])
    data_filled = initLogger(configfile_obj.properties["data"], parameters_complete, parameters_complete["debug"], version)

    return configfile_obj, path_configfile

def run_DGINN():
    print("RUN DGINN")
    ### execute command "snakemake -c1 -p --use-singularity"
    run_snakemake = os.popen('cd ./instance/DGINN && snakemake -c20 -p --latency-wait 1000 --use-singularity').read()
    return run_snakemake

@app.route('/rerun', methods=['GET'])
def rerun():
    ### execute command "snakemake -F -c1 -p --use-singularity"
    stream = os.popen('cd ./instance/DGINN && snakemake --unlock && snakemake -F -c10 -p --use-singularity &')
    output = stream.read()
    return output

@app.route('/stop', methods=['GET'])
def stop():
    ### execute command "killall -TERM snakemake"
    stream = os.popen('killall -TERM snakemake')
    output = stream.read()
    return "Stopped"

# service for download
@app.route('/download', methods=['GET'])
def download():
    path_instance = request.args.get("file")
    return send_file(path_instance, as_attachment=True)

@app.route('/view', methods=['GET'])
def view():
    path_instance = request.args.get("file")
    return render_template('viewTree.html', file=path_instance)

@app.route('/phylotree', methods=['GET'])
def phylotree():
    path_tree = request.args.get("file")
    dir = path_tree.split("/results/")[0]
    treefile = open(path_tree,"r")
    tree_xml = path_tree.split(".")[0]
    result = ""
    if path_tree.endswith("phylip_phyml_tree.txt"):
        for line in treefile:
            tline = re.split(' ',line)
            newick=tline[0]
            result = result + createPhyloXML(newick, dir) + "\n"
    elif path_tree.endswith(".nhx"):
        Phylo.convert(path_tree, "newick", tree_xml+".xml", "phyloxml")
        result = Phylo.read(tree_xml+".xml","phyloxml")
    return Response(result, mimetype='text/xml')

### research the documents by time stamp
@app.route('/get_instances', methods=['GET'])
def get_instances():
    instances = os.listdir('./instance')
    instances.sort()
    return instances

### present the result files
@app.route('/get_results/<instance>', methods=['GET'])
def get_results(instance):
    res = []
    base_path = f"./instance/{instance}/results"
    for (root,dirs,files) in os.walk(base_path):
        for file in files:
            res.append(classify(f"./instance/{instance}/config",{"path":os.path.join(root, file), "name":file}))
        # for directory in dirs:
        #     res.append({"path": os.path.join(root, directory), "name": directory + '/'})

    return res

### func exists and classify is for classifying all the files by processus
def exists(collection, func):
    if isinstance(collection, dict):
        for key, value in collection.items():
            if func(key) and func(value):
                return True
    else:
        for item in collection:
            if func(item):
                return True
    return False

def classify(config_path, obj):
    with open(config_path+'/configfile.json', 'r') as json_in:
        json_dict = json.load(json_in)

    data = json_dict["data"]
    
    if obj['name'].startswith('dup_') or obj['name'].endswith('_recs.nhx') or obj['name'].endswith('_recs.svg') or \
        obj['name'].endswith('.best.fas') or obj['name'].endswith('_stats.txt') or\
        ('duplication' in data and exists(data['duplication'], lambda x: obj['name'] in x)):
        obj['classify'] = 'Duplication'
    elif obj['name'].startswith('align_'):
        obj['classify']='Alignment'
    elif obj['name'].startswith('blast_') or obj['name'].startswith('accessions_') or obj['name'].startswith('fasta_input_'):
        obj['classify']='BLAST'
    elif obj['name'].startswith('orf_'):
        obj['classify']='ORF'
    elif obj['name'].startswith('tree_'):
        obj['classify']='Phylogenetic Tree'
    elif obj['name'].startswith('recomb'):
        obj['classify']='Recombination'
    elif obj['name'].startswith('possel') or obj['name'].endswith('.bpp'): 
        obj['classify']='Positive Selection'
    elif 'dAlTree' in data and isinstance(data['dAlTree'], dict) and any(obj['name'] in value for value in data['dAlTree'].values()):
        obj['classify'] = 'Duplication'
    else: obj['classify']='other'
    return obj

### desplay the percentage from the process of the rules
@app.route('/progression', methods=['GET'])
def progression():
    if Path('./instance/DGINN/.snakemake/log').exists():
        percentage = os.popen('cat ./instance/DGINN/.snakemake/log/* | grep -i "[0-9]* of [0-9]* steps ([0-9]*%) done" | grep -oP \([0-9]*%\) | tail -1').read()
        if len(percentage) == 0:
            return "0%"
        return percentage
    return "0%"

### polling file log to see the processus
@app.route('/log', methods=['GET'])
def polling():
    if Path('./instance/DGINN/.snakemake/log').exists():
        paths = sorted(Path('./instance/DGINN/.snakemake/log').iterdir(), key=os.path.getmtime)
        if len(paths) == 0 : 
            return ""
        log = paths[len(paths)-1]
        
        content = ""
        with open(log, 'r') as reader:
            for line in reader.readlines():
                content += line
        return content
    return ""

