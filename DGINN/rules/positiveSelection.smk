# rule for calculate positive selection from positiveSelection.py

rule positiveSelection:
    input:
        resDir+"tree_input_{filename}.fasta" if (parameters["step"]=="blast" and not parameters["recombination"] and not parameters["duplication"]) \
        else resDir+"align_{filename}.fasta" if (parameters["step"]=="option" and not parameters["recombination"] and not parameters["duplication"]) \
        else resDir+"dup_{filename}_files_list.txt",
        resDir+"tree_{filename}_filtered2.phylip_phyml_tree.txt" if parameters["step"] == "blast" else resDir+treename+".tree"
    output:
        resDir+"possel_{filename}_files_list.txt" 
    threads: 8
    params:
        config = config_file
    message: "\n Step - Positive Selection - writing output in 'possel_{filename}_files_list.txt'" 
    run:
        shell("python3 scripts/positiveSelection.py {params.config}")