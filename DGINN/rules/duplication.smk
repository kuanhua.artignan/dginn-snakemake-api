# rule duplication from the script Duplication.py : fast, tree -> nhx
rule duplication:
    input:
        resDir+"tree_input_{filename}.fasta" if (parameters["step"]=="blast" and not parameters["recombination"]) \
        else resDir+"align_{filename}.fasta" if (parameters["step"]=="option" and not parameters["recombination"]) \
        else resDir+"recomb_{filename}_files_list.txt",
        resDir+"tree_{filename}_filtered2.phylip_phyml_tree.txt" if parameters["step"]=="blast" else resDir+treename+".tree"
    params:
        config = config_file
    output:
        resDir+"dup_{filename}_files_list.txt"
    threads: 6
    message: "\nStep Duplication - writing output in 'dup_{filename}_recs2.nhx" 
    shell:
        "python3 scripts/duplication.py {params.config} {output}"