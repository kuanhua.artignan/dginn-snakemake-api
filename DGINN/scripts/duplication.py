import sys, os, json, shutil, logging
# get the path of the directory containing the current script
script_dir = os.path.dirname(os.path.abspath(__file__))
# construct the path of the module's parent directory
module_dir = os.path.join(script_dir, '..', 'lib')
# add the module's parent directory to the system path
sys.path.append(module_dir)
from analyseFun import checkPhyMLTree
from loadFile import spTreeCheck, duplPSEntry
from treeFun import treeTreatment

def process_data(config_dict, data, parameters):
    if parameters["duplication"] and parameters["recombination"]:

        for aln in data["recomb_files"]:
            data["aln"]=aln
            data, data["dAlTree"] = duplPSEntry(data)
            dTree = data["dAlTree"].pop(data["aln"])
            spTreeCheck(data, "duplication", parameters["duplication"])
            data["dAlTree"][data["aln"]] = dTree

            dAlTree_after_check = checkPhyMLTree(data, data["dAlTree"], parameters["nbspecies"], parameters["LBopt"])
            dAlTree_after_treatment = treeTreatment(data, parameters, dAlTree_after_check, sys.argv[2])

            data["dAlTree"] = dAlTree_after_check
            data["dAlTree"].update(dAlTree_after_treatment)

    elif parameters["duplication"] and not parameters["recombination"]:
        data, data["dAlTree"] = duplPSEntry(data)
        dTree = data["dAlTree"].pop(data["aln"])
        spTreeCheck(data, "duplication", parameters["duplication"])
        data["dAlTree"][data["aln"]] = dTree

        dAlTree_after_check = checkPhyMLTree(data, data["dAlTree"], parameters["nbspecies"], parameters["LBopt"])
        dAlTree_after_treatment = treeTreatment(data, parameters, dAlTree_after_check, sys.argv[2])

        data["dAlTree"] = dAlTree_after_check
        data["dAlTree"].update(dAlTree_after_treatment)
    
    config_dict["parameters"] = parameters
    config_dict["data"] = data

    filename = data["o"] + "dup_" + parameters["infile"].split("data/")[1].split(".")[0]
    write_file_list(filename, data["duplication"])

    with open(sys.argv[1], 'w') as config_out:
        json.dump(config_dict, config_out, indent="")

def write_file_list(filename, files):
    with open(filename + "_files_list.txt", "w") as file_list:
        for file in files:
            file_list.write(file + "\n")

if __name__ == "__main__":
    with open(sys.argv[1], 'r') as config_in:
        config_dict = json.load(config_in)

    parameters = config_dict["parameters"]
    data = config_dict["data"]

    if parameters["step"] == "option":
        process_data(config_dict, data, parameters)
    else:
        if parameters["duplication"] and not parameters["recombination"]:
            data, data["dAlTree"] = duplPSEntry(data)
            dTree = data["dAlTree"].pop(data["aln"])
            spTreeCheck(data, "orf", parameters["duplication"])
            data["dAlTree"][data["aln"]] = dTree

            dAlTree_after_check = checkPhyMLTree(data, data["dAlTree"], parameters["nbspecies"], parameters["LBopt"])
            dAlTree_after_treatment = treeTreatment(data, parameters, dAlTree_after_check, sys.argv[2])

            data["dAlTree"] = dAlTree_after_check
            data["dAlTree"].update(dAlTree_after_treatment)
        
        elif parameters["duplication"] and parameters["recombination"]:

            for aln in data["recomb_files"]:
                data["aln"]=aln
                data, data["dAlTree"] = duplPSEntry(data)
                dTree = data["dAlTree"].pop(data["aln"])
                spTreeCheck(data, "duplication", parameters["duplication"])
                data["dAlTree"][data["aln"]] = dTree

                dAlTree_after_check = checkPhyMLTree(data, data["dAlTree"], parameters["nbspecies"], parameters["LBopt"])
                dAlTree_after_treatment = treeTreatment(data, parameters, dAlTree_after_check, sys.argv[2])

                data["dAlTree"] = dAlTree_after_check
                data["dAlTree"].update(dAlTree_after_treatment)

    config_dict["parameters"] = parameters
    config_dict["data"] = data

    filename = data["o"] + "dup_" + parameters["infile"].split("data/")[1].split(".")[0]
    write_file_list(filename, data["duplication"])

    with open(sys.argv[1], 'w') as config_out:
        json.dump(config_dict, config_out, indent="")