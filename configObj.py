import sys, os, json, shutil, logging
# get the path of the directory containing the current script
script_dir = os.path.dirname(os.path.abspath(__file__))
# construct the path of the module's parent directory
module_dir = os.path.join(script_dir,'DGINN','lib')
# add the module's parent directory to the system path
sys.path.append(module_dir)

from initConfig import paramDef, initLogger

from Bio import SeqIO

class Configuration:

    properties = {
        "parameters": {
            "infile": "",
            "queryName": "",
            "queryFile": "",
            "outdir": "",
            "logfile": "",
            "step": "blast",
            "blastdb": "nr",
            "evalue": 0.001,
            "mincov": 50,
            "percID": 70,
            "maxLen": "cutoff(3)",
            "entryQuery": "Primates[Organism]",
            "remote": True,
            "APIKey": "",
            "phymlOpt": "",
            "sptree": "",
            "duplication": False,
            "LBopt": "cutoff(50)",
            "nbspecies": 8,
            "recombination": False,
            "positiveSelection": False,
            "hyphySeuil": 0.1,
            "busted": False,
            "meme": False,
            "models": "M0",
            "paml": False,
            "bppml": True,
            "mixedlikelihood": True,
            "opb": False,
            "gnh": False,
            "alnfile": "",
            "treefile": "",
            "alnformat": "Fasta",
            "basename": "",
            "debug": False,
            "align_codon": [],
            "align_nt": True
        },
        "data": {
            "queryFile": "",
            "o": "",
            "db": "",
            "geneName": "",
            "sequence": "",
            "blastRes": "",
            "lBlastRes": [],
            "sptree": "",
            "ORFs": "",
            "aln": "",
            "tree": "",
            "queryName": "",
            "alnFormat": "",
            "baseName": "",
            "dAlTree": {},
            "firstStep": "orf",
            "cor": "",
            "accnFile": "",
            "seqFile": "",
            "recomb_files": [
            "results/fileOne.fasta",
            "results/fileTwo.fasta"
            ]
        }
    }

    # Path or list of paths (absolute or relative) to the files needed to start the pipeline. : string
    def setInputFile(self, file):
        self.properties["parameters"]["infile"] = file

    def getInputFile(self):
        return self.properties["parameters"]["infile"]
    
    def getFileName(self):
        return str(self.properties["parameters"]["infile"].split("data/")[1].split(".")[0])

    def setAlnFile(self, file):
        self.properties["parameters"]["infile"] = file
        self.properties["parameters"]["alnfile"] = file
        self.properties["data"]["aln"] = file

    def getQueryName(self, file_path):
        file = open(file_path, 'r').readlines()
        for line in file:
            filename = (line.split(">"))[1].strip() if line.startswith(">") == True else ""
            return str(filename)

    def setTreeFile(self, file_path):
        self.properties["parameters"]["treefile"] = file_path
        self.properties["data"]["tree"] = file_path
    
    def getTreeName(self):
        return str(self.properties["parameters"]["treefile"].split("data/")[1].split(".")[0])
    
    def setSpTreeFile(self, file_path):
        self.properties["parameters"]["sptree"] = file_path

    # Output directory for all results
    def setOutDir(self, outdir_path):
        self.properties["parameters"]["outdir"] = outdir_path

    def getOutDirPath(self):
        return self.properties["parameters"]["outdir"]
    
    def setStep(self, step):
        self.properties["parameters"]["step"] = step

    def writeObjects(self):
        return json.dumps(self.properties, indent=4)
    
    def is_duplication(self, boolean):
        self.properties["parameters"]["duplication"] = boolean

    def is_recombination(self, boolean):
        self.properties["parameters"]["recombination"] = boolean

    def is_positiveSelection(self, boolean):
        self.properties["parameters"]["positiveSelection"] = boolean

    # E-value for Blast (default 10⁻⁴)
    def setEvalue(self, value):
        self.properties["parameters"]["evalue"] = value
    
    # Coverage for Blast (default 50) : int
    def setCoverageBlast(self, value):
        self.properties["parameters"]["mincov"] = value
    
    # Percentage of identity for Blast (default 70) : int
    def setPerIdentityBlast(self, value):
        self.properties["parameters"]["percID"] = value

    # Option for eliminating overly long sequences (default cutoff(3)) : string
    def setCutLongSeq(self, cutoff):
        self.properties["parameters"]["maxLen"] = cutoff
    
    # Determines if Blast is performed against NCBI databases (default = True)
    #def setRemote(self):
    #    self.properties["parameters"]["remote"] = True

    # NCBI API key to increase Blast speed, obtainable from the NCBI : string
    def setAPIKey(self, key):
        self.properties["parameters"]["APIKey"] = key
    
    def setAlignCodon(self, methods):
        if len(self.properties["parameters"]["align_codon"])==0:
            self.properties["parameters"]["align_codon"].append(methods) 
    
    def setAlignNT(self, boolean):
        self.properties["parameters"]["align_nt"] = boolean

    def setFirstStep(self, firstStep):
        self.properties["data"]["firstStep"]=firstStep
    
def setConfigFile(configfile,infile,Snakefile):
    config = Configuration()
    configfilesuffix = config.getFileName(infile)
    shutil.copy(configfile, configfile+"_"+configfilesuffix)
    with open(Snakefile, "r") as f:
        new_Snakefile = f.readlines()


if __name__ == "__main__" :
    
    
    configfileJson = Configuration()
    # duplication, recombination, positiveSelection
    configfileJson.is_duplication(True)
    configfileJson.is_recombination(False)
    configfileJson.is_positiveSelection(True)

    # inputfileLists = ["ex_CCDS.fasta", "APOBEC3_primates.fasta"]
    # inputTreefile = ["ex_geneTree.tree", "APOBEC3_primates-PhyML.tree"]
    # inputSPTree = ["ex_spTree.tree"]
    
    file_path = "/home/etudiant/Documents/DGINN-API/dginn-api-stage/DGINN/data/ex_aln.fasta"
    tree_path = "/home/etudiant/Documents/DGINN-API/dginn-api-stage/DGINN/data/ex_geneTree.tree"
    sptree_path = "/home/etudiant/Documents/DGINN-API/dginn-api-stage/DGINN/data/ex_spTree.tree"
    config = "/home/etudiant/Documents/DGINN-API/dginn-api-stage/DGINN/config/configfile.json"
    outdir = "/home/etudiant/Documents/DGINN-API/dginn-api-stage/DGINN/results/"
    
    configfileJson.setSpTreeFile(sptree_path)
    configfileJson.setTreeFile(tree_path)
    treename = configfileJson.getTreeName()
    

    if not os.path.exists(outdir):
        os.makedirs(outdir)
        configfileJson.setOutDir(outdir)
    configfileJson.setOutDir(outdir)

    if len(list(SeqIO.parse(file_path,'fasta'))) > 1:
        configfileJson.setAlnFile(file_path)
        filename = configfileJson.getFileName()
        configfileJson.setStep("option")
        version="1.0"
        parameters_complete = paramDef(configfileJson.properties["parameters"])
        data_filled = initLogger(configfileJson.properties["data"], parameters_complete, parameters_complete["debug"], version)
        objectJson = configfileJson.writeObjects()
        open(config, 'w').write(objectJson)
        shutil.copy(file_path, outdir+filename+'.fasta')
        shutil.copy(tree_path, outdir+treename+'.tree')

    else:
        configfileJson.setInputFile(file_path)
        filename = configfileJson.getFileName()
        configfileJson.setStep("blast")
        configfileJson.setAPIKey("d636d39ffc08e83d78812daac80693ef9108")
        configfileJson.setAlignCodon("macse")
        version="1.0"
        parameters_complete = paramDef(configfileJson.properties["parameters"])
        data_filled = initLogger(configfileJson.properties["data"], parameters_complete, parameters_complete["debug"], version)
        objectJson = configfileJson.writeObjects()
        open(config, 'w').write(objectJson)
        shutil.copy(file_path, outdir+'blast_input_'+filename+'.fasta')



