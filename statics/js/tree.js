function dessineMoiUnArbre(inputData){

  var tagSimpleGene = true;	
  //  Variables globales :
  // =====================
  var svg = d3.select("#mainsvg");          // svg principal
  var gtree = svg.append("g");              // groupe arbre
  var cartesvg = d3.select("#cartesvg");    // Selectionne la div cartesvg
  var carteg = cartesvg.append("g");        // groupe carte
  var waiting = d3.select("#waiting");      // div waiting
  var divcarte = d3.select("#carte")
    .append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);           // div divcarte pour l'affichage mouseover
  var flag_pointer = 0;             // Pointeur actif
  var fixed = 1;                    // Ecart entre pointeur:  1 = libre, -1 = fixed
  var minuse1 = cartesvg.append("use") // Pos max du pointeur de gauche
    .attr("id", "minuse1")
    .attr("href", "#pointer")
    .attr("x", 1)
    .attr("y", 50)
    .attr("fill", "white")
    .attr("stroke", "#556B2F")
    .attr("stroke-width", "1px");
  var maxuse2 = cartesvg.append("use") // Pos max du pointeur de droite
    .attr("id", "maxuse2")
    .attr("href", "#pointer")
    .attr("x", 100)
    .attr("y", 50)
    .attr("fill", "white")
    .attr("stroke", "#B22222")
    .attr("stroke-width", "1px");
  var use1 = cartesvg.append("use") // Pointeur de gauche
    .attr("id", "use1")
    .attr("href", "#pointer")
    .attr("x", 1)
    .attr("y", 50)
    .attr("fill", "#556B2F")
    .attr("stroke", "#039BE5")
    .attr("stroke-width", "1px");
  var use2 = cartesvg.append("use") // Pointeur de droite
    .attr("id", "use2")
    .attr("href", "#pointer")
    .attr("x", 100)
    .attr("y", 50)
    .attr("fill", "#B22222")
    .attr("stroke", "#039BE5")
    .attr("stroke-width", "1px");

  var dragHandler = d3.drag() // Gestion du pointeur de gauche
    .on("start", function () {
      flag_pointer = 1;
    })
    .on("end", function (event) {
      flag_pointer  = 0;                 // Libere le pointeur
      bloc_syntenies = calculate_bloc(); // Recalcule les blocs
      if (fixed == -1) { // fixed
            d3.selectAll("#maxuse2").attr("x", lastGene / scale_carte);
            d3.selectAll("#minuse1").attr("x", firstGene / scale_carte);
      }
      else { // free
        d3.selectAll("#minuse1").attr("x", Math.max(1,lastGene - maxEcart)  / scale_carte);
        d3.selectAll("#maxuse2").attr("x", Math.min(genome_length[referenceGenome],firstGene  + maxEcart) / scale_carte);
      }
      updateLayout()
    })
    .on("drag", function (event) {
      // Conditions pour deplacer:
      if (((event.x < lastGene / scale_carte - 5) || (fixed  == -1))
      && (event.x > 0)
      && (( lastGene +  (event.x * scale_carte - firstGene) <  genome_length[referenceGenome] ) ||  (fixed  == 1))
      && ((( lastGene - event.x  * scale_carte) / (zoomLevel ) < max_nb_of_blocks  ) || (fixed  == -1)))
      {
        d3.select(this).attr("x", event.x); // deplace le pointeur gauche
        if (fixed  == -1) {
          // Nouveau geneMax si ecart fixe
          lastGene = lastGene +  (event.x * scale_carte - firstGene);
        }
        // Nouveau geneMin ( a calculer apres geneMax)
        firstGene = event.x * scale_carte;
        if (fixed  == -1) {
          // Deplace le pointeur droit
          d3.selectAll("#use2").attr("x", lastGene / scale_carte);
        }
      }
    }
  );
  dragHandler(cartesvg.selectAll("#use1"));

  var dragHandler2 = d3.drag()   // Gestion du pointeur de droite
    .on("start", function () {
      flag_pointer = 1;
    })
    .on("end", function () {
      flag_pointer  = 0; // Je libere le pointeur
      bloc_syntenies = calculate_bloc();
      if (fixed == -1) { //fixed
        d3.selectAll("#maxuse2").attr("x", lastGene / scale_carte);
        d3.selectAll("#minuse1").attr("x", firstGene / scale_carte);
      }
      else { // free
        d3.selectAll("#minuse1").attr("x", Math.max(1,lastGene - maxEcart)  / scale_carte);
        d3.selectAll("#maxuse2").attr("x", Math.min(genome_length[referenceGenome],firstGene  + maxEcart) / scale_carte);
      }
      updateLayout(cladeRoot);
    })
    .on("drag", function (event) {
      // Conditions pour deplacer:
      if (((event.x > firstGene / scale_carte) ||  (fixed  == -1))
      && (( firstGene +  (event.x * scale_carte - lastGene) > 0 ) ||  (fixed  == 1))
      && ( event.x <= genome_length[referenceGenome] / scale_carte)
      && ((( event.x  * scale_carte - firstGene ) / (zoomLevel ) < max_nb_of_blocks  ) || (fixed  == -1))
      ){
        d3.select(this).attr("x", event.x); // Je deplace le pointeur droit
        if (fixed  == -1) {
          // Nouveau geneMin si ecart fixe
          firstGene = firstGene +  (event.x * scale_carte - lastGene);
        }
        // Nouveau geneMax ( a calculer apres geneMin !)
        lastGene = event.x * scale_carte;
        if (fixed  == -1) {
          d3.selectAll("#use1").attr("x", firstGene / scale_carte); // Je deplace le pointeur gauche
          }
        }
    }
  );
  dragHandler2(cartesvg.selectAll("#use2"));

  const min_blocksize = 5; // Largeur en pixel d'un bloc de genes
  var nodeWidth = 5000;       // Largeur du noeud
  var nodeHeight = 20;      // Hauteur du noeud
  var step = 10;
  var minNodeHeight = 6;   // Hauteur minimum du noeud
  var block_height = 4;    // Hauteur en pixel d'un bloc de genes
  var chromo_block_height = 4;    // Hauteur en pixel d'un bloc de genes
  const scale_carte = 15.0;  // Echelle de la carte : numero de gene / position
  var shift_text = 200;     // Decalage en X du bloc de genes par rapport a la feuille
  var symbol = d3.symbol().size(16);   // Symbol des noeuds
  var myColor = [];        // Tableau des échelles de couleurs
  myColor.push(d3.scaleLinear().domain([1,100]).range(["white", "blue"]));
  myColor.push(d3.scaleLinear().domain([1,100]).range(["blue", "white"]));
  myColor.push(d3.scaleLinear().domain([1,100]).range(["green", "red"]));
  myColor.push(d3.scaleLinear().domain([1,100]).range(["red", "green"]));
  var colour_scale_index = 0;    // Choix de l'échelle de couleur
  var chromo_carte = [];   // Tableau des chromosomes
  var cladeRoot;           // Arbre parsé par xml
  var globalRoot;          // Copie de cladeRoot
  var _cladeRoot = null;   // Valeur alternative utilisée pour l'action subtree (focus)
  var Parent ;             // Variable utilisée pour l'action subtree (focus)
  var _Parent = null;      // Variable utilisée pour l'action subtree (focus)
  var bloc_syntenies;      // Dico:  genome -> blocs de genes
  var absolute_syntenies;  // Dico:  genome -> genes ancestraux
  var equivalences;        // Dico:  genome -> dico : gene ancetsral -> description
                          // gene actuel
  var relatives_syntenies; // Dico:  genome -> genes actuels ordones
                          // selon l'ordre des genes ancestraux du genome de reference
  var genomesWC = new Object(); // Genomes sans chromosomes (without chromosome)
  var chromo_only = 1;      // flag affiche seulement les assemblages avec chromomsomes
  var display_chromo = 1;   // flag affiche les chromosomes
  var referenceGenome = "Homo_sapiens";     // Genome de reference
  var max_nb_of_blocks_total  = 250 * 250;  // Nombre total max d'objets 'bloc de gene'
  var max_nb_of_blocks = 250;               // Nombre max d'objets 'bloc de gene'
                                            // par genome
  var genome_length = new Object(); // Dico : genome -> longueur genome
  var longest_sequence = 0;         // Sequence la plus longues
  var total_nb_leaves  = 0;         // Nombre de feuilles
  var todo = 0; // Nombre de fichiers a charger
  var done = 0; // Nombre de fichiers charges
  var firstGene = 1;      // Premier gene
  var maxEcart = 0;       // Ecart entre premier et dernier gene
  var lastGene = 20000 ;  // Dernier gene (valeur initiale bidon)
  var zoomLevel = 100 ;   // Nombre de genes par bloc
  var disp_intnode = 1;   // Options d'affichage
  var disp_leaves = "";
  var chromosome_colors = ["grey","red","blue","green","yellow","pink","orange","magenta","maroon","skyblue","aquamarine"]
  //  action par defaut
  var action;
  var action_description = "Collapse/Expand";

  // Fonction(s) Transformation des donnees en cladograme:
  var fnlayout = {};
  fnlayout.cladogram = function () {
    var layout = d3.cluster();
    var layoutCladogram = function (root) {
      layout(root);
      root.each(function (d) {
        var temp = d.x;
        d.x = d.y;
        d.y = temp;
      });
    }
    layoutCladogram.nodeSize = function (x) {
      var temp = x[0];
      x[0] = x[1];
      x[1] = temp;
      layout = layout.nodeSize(x);
      return layoutCladogram;
    }
    layoutCladogram.Size = function (x) {
      layout = layout.Size(x);
      return layoutCladogram;
    }
    return layoutCladogram;
  }
  var layout = fnlayout.cladogram();

  // Fonctions Gestion des action sur les noeuds
  var svgEvents = {};

  // Inversion des enfants du noeud
  svgEvents.switchChildren = function(d) {
    if (d.children[0] && d.children[1]) {
      var temp = d.data.clade[0];
      d.data.clade[0] = d.data.clade[1];
      d.data.clade[1] = temp;
      updateLayout();
    }
  }

  // Ferme/Ouvre le noeud
  svgEvents.collapse = function(d) {
    if (d.data.clade) {
      d.data._clade = d.data.clade;
      d.data.clade = null;
      d.data.nodeinfo = {status : "collapsed"};
    }
    else if (d.data._clade) {
      d.data.clade = d.data._clade;
      d.data._clade = null;
      d.data.nodeinfo = {status : "extended"};
    }
    var _treeRoot = d3.hierarchy(cladeRoot, function(d) {
        return d.clade;
    });
    var _nodes = _treeRoot.descendants();
    var _leaves = _nodes.filter(function (e) {
        return !e.children
    });
    //  le nombre de feuilles a change, donc le nombre de blocs max
    // var nb_leaves =  _leaves.length;
    // max_nb_of_blocks = Math.round(max_nb_of_blocks_total / (nb_leaves + 1 ));
    /* First and last genes*/
    // firstGene = 1;
    // lastGene = Math.min(firstGene + max_nb_of_blocks * zoomLevel, genome_length[referenceGenome]);
    // d3.selectAll("#use1").attr("x", firstGene / scale_carte);
    // d3.selectAll("#use2").attr("x", lastGene / scale_carte);
    // bloc_syntenies = calculate_bloc();
    updateLayout();
    window.scrollTo(0, 0);
  }
  // svgEvents.openall = function(n) {
  //   var fils = n.descendants();
  //   fils.forEach(function (d) {
  //     if   (!d.data.clade) {
  //       d.data.clade = d.data._clade;;
  //     }
  //   })
  //   updateLayout();
  // }

  // Sous-arbre
  svgEvents.focus = function(d) {
    if(d.data != cladeRoot) {
      var parent = d.parent;
      _cladeRoot = parent.data;
      _Parent = parent;
      cladeRoot = d.data;
    }
    else {
      var parent = _Parent;
      cladeRoot = parent.data;
      _Parent = parent.parent;
    }
    var _treeRoot = d3.hierarchy(cladeRoot, function(d) {
      return d.clade;
    });
    var _nodes = _treeRoot.descendants();
    var _leaves = _nodes.filter(function (e) {
      return !e.children
    });
    //  le nombre de feuilles a change, donc le nombre de blocs
    // var nb_leaves =  _leaves.length;
    // max_nb_of_blocks = Math.round(max_nb_of_blocks_total / (nb_leaves + 1 ));
    // /* First and last genes*/
    // firstGene = 1;
    // lastGene = Math.min(firstGene + max_nb_of_blocks * zoomLevel, genome_length[referenceGenome]);
    // d3.selectAll("#use1").attr("x", firstGene / scale_carte);
    // d3.selectAll("#use2").attr("x", lastGene / scale_carte);
    // bloc_syntenies = calculate_bloc();
    updateLayout();
    window.scrollTo(0, 0);
  }

  // Choix du genome de reference
  svgEvents.reference = function(d) {
    var proposedGenome = d.data.name;
    if (proposedGenome in absolute_syntenies) {
      referenceGenome = d.data.name;
      /* Syntenies relatives */
      relatives_syntenies = calculate_relative();
      /* First and last genes*/
      firstGene = 1;
      lastGene = Math.min(firstGene + max_nb_of_blocks * zoomLevel, genome_length[referenceGenome]);
      maxEcart = lastGene - firstGene + 1;
      d3.selectAll("#use1").attr("x", firstGene / scale_carte);
      d3.selectAll("#use2").attr("x", lastGene / scale_carte);
      /* Blocs*/
      bloc_syntenies = calculate_bloc();
      updateData();
      updateLayout();
    } else {
      alert("This genome has no data and can not be used as a reference");
    }
  }

  //  action par defaut
  action = svgEvents.collapse;

  // Fonction Genere les chemins pour un arbre de type diagonal
  var diagonal = function(d) {
    var path = d3.path();
    path.moveTo(d.source.x, d.source.y);
    path.lineTo(d.source.x, d.target.y);
    path.lineTo(d.target.x, d.target.y);
    return path.toString();
  }

  // Fonction qui recupere la liste des genomes dans chromo
  const genomeWCList = async function getGenomesWithoutChromo() {
      return await fetch('/phylotree');
  }

  // Fonction qui recupere la liste des genomes
  const genomeList = async function getGenomes() {
      return await fetch('/phylotree');
  }


  // Fonction de calcul de la position  d'un noeud a partir des longueurs
  // de branch_length
  // --------------------------------------------------------------------
  function phylo(n) {
    var dist = 0.0;
    if (n && n.data.branch_length != null) {
      var p = n.parent;
      dist = parseFloat(n.data.branch_length) + phylo(p);
      return dist;
    }
    return dist;
  }

  // Fonnction pour utiliser les lobgueurs de branchede lors de l'affichage
  // d'un arbre
  // ----------------------------------------------------------------------
  function phylogeny(treeRoot,offset) {
    treeRoot.each(function (d) {
      var phylodist = phylo(d);
      d.x = phylodist*offset;
    });
  }
  function phylogeny_setloss(treeRoot,offset) {
    treeRoot.each(function (d) {
      if (d.data.lastEvent.eventType === "loss") {
       var lospar = d.parent;
       d.x = (d.x+lospar.x)/2
  }
    });
  }


  // Fonction principale qui affiche l'arbre
  async function drawTree() {
      console.log("DISPLAY TREE");
      d3.select("#menus")
        .style("visibility", "hidden");
      d3.select("#carte")
        .style("visibility", "hidden");
      d3.select("#loading").text("Displaying tree...");
      /* Recupere les données */
      d3.select("#loading").text("Loading syntenies ...");
      console.log("WAIT FOR GENOMES");
      // const genome_list = await genomeList();
      const genome_list = []
      console.log("WAIT FOR GENOMES WITHOUT CHROMO");
      // const genomeWC_list = await genomeWCList();
      const genomeWC_list = [];
      for (var toto in genomeWC_list.data) {
        console.log(genomeWC_list.data[toto].Genome);
        genomesWC[genomeWC_list.data[toto].Genome]=1;
      };

      console.log("WAIT FOR SYNTENIES");
      // const syntenies = await synteniesData(genome_list.data);
      const syntenies = new Object();
      absolute_syntenies = syntenies;
      d3.select("#loading").text("Loading descriptions ...");
      console.log("WAIT FOR DESCRIPTIONS");
      // const descriptions = await descriptionsData(genome_list.data);
      const descriptions = new Object();
      /* Dictionnaire des equivalences ancetre -> description de gene */
      console.log("GENERE EQUIVALENCES");
      d3.select("#loading").text("Generating equivalences ...");
      // equivalences = genere_equivalences(descriptions.data);
      equivalences = genere_equivalences(descriptions);
      /* Syntenies relatives */
      console.log("CALCULATE RELATIVES");
      // firstGene = 1;
      // lastGene = genome_length[referenceGenome];

      console.log("LAST GENE OF "+referenceGenome + " IS "+lastGene);
      d3.select("#loading").text("Calculating relatives syntenies ...");
      relatives_syntenies = calculate_relative();
      firstGene = 1;
      lastGene = genome_length[referenceGenome];
      maxEcart = lastGene - firstGene + 1;
      console.log("LAST GENE OF "+referenceGenome + " IS "+lastGene);
      /* Blocs*/
      console.log("CALCULATE BLOCS");
      d3.select("#loading").text("Calculating blocs ...");
      bloc_syntenies = calculate_bloc();
      d3.select("#loading").text("Bloc syntenies calculated.");
      /* Arbre */
      waiting.remove();
      d3.select("#carte")
        .style("visibility", "visible");
      d3.select("#menus")
        .style("visibility", "visible");
      // instialise les formulaires et autre div
      $('#long').css('color','red');
      $('#collapse').css('color','red');
      $('#colourscale1').css('color','red');
      $('#zoomlevel2').css('color','red');
      $('#assemblies').css('opacity',0.3);
      d3.selectAll("#use1").attr("x", firstGene / scale_carte);
      d3.selectAll("#use2").attr("x", lastGene / scale_carte);
      d3.selectAll("#minuse1").attr("opacity",1.0);
      d3.selectAll("#maxuse2").attr("opacity",1.0);
      d3.selectAll("#maxuse2").attr("x", Math.min(firstGene + maxEcart, genome_length[referenceGenome]) / scale_carte);
      d3.selectAll("#minuse1").attr("x", Math.max(0,lastGene - maxEcart) / scale_carte);
      $('#fixed').css('opacity',0.3);
      cladeRoot = inputData.phyloxml.phylogeny.clade;
      globalRoot = cladeRoot;
      var _treeRoot = d3.hierarchy(cladeRoot, function(d) {
        return d.clade;
      });
      var _nodes = _treeRoot.descendants();
      var _leaves = _nodes.filter(function (e) {
          return !e.children
      });
      // var nb_leaves =  _leaves.length;
      // total_nb_leaves = nb_leaves;
      // max_nb_of_blocks = Math.round(max_nb_of_blocks_total / (nb_leaves + 1));

      updateData();
      updateLayout();
  }

  // Fonction qui genere les equivalences ancetre -> description du gene
  function genere_equivalences(descriptions) {
      const equivalences =  new Object();
      for (var genome in  descriptions) {
          const equivalence =  new Object();
          var description = descriptions[genome];
          for (var i = 0; i< description.length; i++) {
              var ancetre = description[i].A;
              var name = description[i].N;
              var value = description[i].V;
              var chromosome = description[i].Ch;
              var col_chromo = description[i].C;
              var desc = [name,value,chromosome,col_chromo];
              equivalence[ancetre] = desc;
          }
          equivalences[genome] = equivalence;
      }
      return equivalences
  }

  // Fonction qui genere les blocs
  function calculate_bloc() {
      console.log("start calculate_bloc");
      const bl_syntenies =  new Object();
      for (var genome in  relatives_syntenies) {
          var rel_synteny = relatives_syntenies[genome];
          var len_rel_syn = rel_synteny.length;
          var blocs = [];
          var nb_gene = 0;
          var nb_val = 0; // Nombre de genes dans le bloc utilisés pour calculer la valeur moyenne
          var nb_rupture_chromo = 0 // Nombre de rupture de chromosomes
          var nb_gene_max = zoomLevel;
          var bloc_val = 0;
          var bloc_name = "";
          var bloc_chromosome = "UNDEFINED";
          var bloc_col_chromo = "pink";
          var bloc_opacity = 0;
          for (var i = firstGene; i < Math.min(lastGene,len_rel_syn); i++) {
              var name = rel_synteny[i][0];
              var val = parseFloat(rel_synteny[i][1]);
              if (val >= 0) {
                  bloc_val += val ;
                  nb_val += 1 ;
              }
              bloc_name += name;
              bloc_name += " ";
              if (name != "NO GENE") {
                var _buf = rel_synteny[i][2];
                if (_buf !== bloc_chromosome) {
                  nb_rupture_chromo += 1;
                }
                bloc_chromosome = _buf;
                bloc_col_chromo = rel_synteny[i][3];
              }
              nb_gene += 1;
              if (nb_gene >= nb_gene_max) {
              /* On termibe le bloc, on l'ajoute et on commmence un nouveau bloc*/
                if (nb_val > 0 )  { bloc_val = bloc_val / nb_val }
                if (nb_val == 0 ) {
                  bloc_chromosome = "NO GENE, NO CHROMOSOME";
                  bloc_col_chromo = "white";
                }
                bloc_name = "Genes "+ (i - nb_gene) + "-" + i + " : "+bloc_name;
                bloc_opacity = 1 - nb_rupture_chromo / nb_gene;
                bloc_opacity = bloc_opacity*bloc_opacity;
                bloc_opacity = Math.round(bloc_opacity*10)/10;

                var bloc = [bloc_name,bloc_val,bloc_chromosome+"/"+nb_rupture_chromo+"/"+nb_gene,bloc_col_chromo,bloc_opacity];
                blocs.push(bloc);
                nb_gene = 0;
                bloc_val = 0;
                bloc_name = "";
                nb_val = 0;
                nb_rupture_chromo = 0;
              }
          }
  /*        var bloc_opacity = 10 - 10 * bloc_col_chromo / nb_gene;
          bloc_opacity = Math.round(bloc_opacity/10);*/
          var bloc = [bloc_name,bloc_val,bloc_chromosome,bloc_col_chromo,bloc_opacity];
          blocs.push(bloc);
          bl_syntenies[genome]=blocs;
      }
      console.log("end calculate_bloc");
      return bl_syntenies

  }

  // Fonction qui calcule les syntenies relatives a un genome
  function calculate_relative() {
      console.log("start calculate_relative");
      var gene_syntenies = new Object();
      if (referenceGenome in absolute_syntenies) {
          // les genes ancestraux dans l'ordre chez le genoem de reference
          var refSynteny = absolute_syntenies[referenceGenome];
          for (var genome in  equivalences) {
              var equivalence = equivalences[genome];
              // Recupere la longeur du genoem de reference
              // Un peu bidon :en fait on ne s'en servira
              //  que pour connaitre la longueur du genome
              // de reference (et la on attribue cette
              // longeur a tous les genomes)
              var _glen = refSynteny.length;
              genome_length[genome] = _glen;
              if (_glen > longest_sequence) {
                longest_sequence = _glen;
              }

              var genes = [];
              for (var i = 0; i< refSynteny.length; i++) {
                  var ancestral = refSynteny[i].A;
                  var name = "NO GENE";
                  var val = -2 ;
                  var chromosome = "NO CHROMOSOME";
                  var col_chromo = "black"
                  if (ancestral in equivalence) {
                      // recupere la description [nom du gene, valeur, chromosme, couleur du chromosme]
                      var extant = equivalence[ancestral];
                      name = extant[0];
                      val = extant[1];
                      chromosome = extant[2];
                      // col_chromo = extant[3];
                      col_chromo = chromosome_colors[extant[3]];
                  }
                  // var gene = {name,val};
                  var gene = [name,val,chromosome,col_chromo];
                  genes.push(gene);
              }
          gene_syntenies[genome] = genes;
          }
      }
      console.log("end calculate_relative");
      return gene_syntenies
  }

  // Fonction qui met a jour le layout
  function updateLayout() {
      d3.select("#loading").text("Updating layout, please wait...");
      //  Recupere l'arbre hierachisé
      var treeRoot = d3.hierarchy(cladeRoot, function(d) {
        return d.clade;
      });
      layout.nodeSize([nodeWidth, nodeHeight]);
      layout(treeRoot);
      // updateData();
      if (tagSimpleGene) {
        phylogeny(treeRoot,nodeWidth);
      } else {
        phylogeny_setloss(treeRoot,nodeWidth);
      }

      updateSvg(treeRoot);
      d3.select("#loading").text("");
  }

  //Fonction qui met a jour le svg
  function updateSvg(treeRoot) {
      console.log("UPDATE SVG", treeRoot)
      var  margin =  {top: 10, down: 10, left: 10  , right: 100}
      var nodes = treeRoot.descendants();
      var links = treeRoot.links();
      var minX = d3.min(nodes, function(d) {
        return d.x;
      });
      var maxX = d3.max(nodes, function(d) {
        return d.x;
      });
      // var widthSVG = maxX - minX + (lastGene - firstGene ) / zoomLevel * min_blocksize + 500 + 10;
      var widthSVG = maxX - minX + (20000 - 1 ) / 500 * min_blocksize + 500 + 10;
      var minY = d3.min(nodes, function(d) {
        return d.y;
      });
      var maxY = d3.max(nodes, function(d) {
        return d.y;
      });
      var heightSVG = maxY - minY ;
      var transition = d3.transition();
      var div = d3.select("body").append("div")
          .attr("class", "tooltip")
          .style("opacity", 0);

      svg.attr("width", widthSVG + margin.right + margin.left+600);
      svg.attr("height", heightSVG + margin.top + margin.down);
      // svg.attr("transform", "translate(0,200)");
      gtree.attr("transform", "translate(" + (margin.right - minX) + "," + (margin.top - minY) + ")")

      // ===== ARETES DE L'ARBRE ======
      // Gere les svg correpondant aux aretes de l'arbre
      var link = gtree.selectAll(".link").data(links);  // associe l'element link a la donnee links
      // Gere les elements qui ont disparu
      link.exit().remove();
      // Gere les elements nouveaux
      var linkEnter =
      link
      .enter()
      .append("path")
      .attr("class", "link");
      linkEnter
      .merge(link)  // fusionne les nouveaux elements avec les anciens
      .attr("fill","none")
      .attr("stroke-width","1x")
      .attr("stroke","blue")
      .attr("d", diagonal);

      // ===== NOEUDS DE L'ARBRE ======
      // D3:  Objets NODE
      // ----------------
      var node = gtree.selectAll(".node").data(nodes);
      //EXIT
      node.exit().remove();
      //ENTER
      var nodeEnter =
      node
      .enter()
      .append("g")
      .attr("class", "node");
      nodeEnter
      .append("g")
      .attr("class", "gsymbol")
      .append("path")
      .attr("class", "symbol");
      nodeEnter
      .append("text")
      .attr("class", "label");
      //ENTER + UPDATE
      var allNodes =
      nodeEnter
      .merge(node)
      .attr("transform", function(d) {
          return "translate(" + [d.x, d.y] + ")";
      })
      .on("click", function(event, d) {
        div.transition()
        .duration(500)
        .style("opacity", 0);
        if (!((d.children || d.data.nodeinfo) && action_description == "Choose as reference")){
          action(d)
        }
      })
      .on('mouseover',function (event, d){
        div.transition()
        .duration(200)
        .style("opacity", .9);
        div.html(function() {
          if (d.children && action_description == "Choose as reference"){
            return "on click:<br>"+action_description +"<br>[ forbidden ]";
          }
          else if (d.data.nodeinfo && action_description == "Choose as reference") {
            // Quele noeud soit collapsed ou extended qu'importe,
            // si nodeinfo existe  cela veut dire que c'est un noeud interne
            return "on click:<br>"+action_description +"<br>[ forbidden ]";
          }
          else {
            return "on click:<br>"+action_description +"<br>[" + d.data.name+"]";
          }
        })
        .style("left", (event.pageX) + "px")
        .style("top", (event.pageY - 28) + "px")
      })
      .on('mouseout',function (d){
        div.transition()
        .duration(500)
        .style("opacity", 0);
      });

      allNodes
      .select(".label")
      .transition()
      .attr("y", function(d) {
        return d.children ? -3 : 3;
      })
      .attr("x", function(d) {
        return d.children ? -3 : 8;
      })
      .style("text-anchor", function(d) {
        return d.children ? "end" : "start";
      })
      .text(function(d) {
        var name = "";
        if ((disp_intnode == 1) && (d.children || d.data.nodeinfo)){
          name = d.data.name;
        }
        if (!(d.children || d.data.nodeinfo)) {
          var bufname = d.data.name.split("_") ;
          name = bufname[0]+" "+bufname[1];
          if (disp_leaves == "short") {
            name = bufname[0];
          }
          if (disp_leaves == "code") {
            name = bufname[0].substring(0, 1)+bufname[1].substring(0, 1);

          }
        }
        return name;
      })
      .style("font-weight",function (d) {
          var fw = "normal";
          if (d.data.name === referenceGenome) {
            fw = "bold";
          }
          return(fw)
      })
      .style("opacity",function (d) {
          var opa = 1.0;
          if ((d.data.name in genomesWC) && chromo_only) {
            opa = 0.3;
          }
          return(opa)
      })
      .style("fill",function (d) {
              var col = "black";
              if (d.data.name === referenceGenome) {
                col = "red";
              }
              return(col)
          });

      allNodes
      .select(".symbol")
      .attr("d", function(d) { //Type de symbol
        if (d.data.nodeinfo) {
            if (d.data.nodeinfo.status === "collapsed") {
              return symbol.type(d3.symbolCircle)()
            }
        }
      });

      var leaves = nodes.filter(function (e) {
          return !e.children
      });



      // ===== BLOCS DE GENES ======
      // Construit le jeu de donnes de bloc de genes
      var genedataset = [];
      leaves.forEach (function (d,i ){
          var yfam = d.y;
          var xfam = d.x;
          if ((d.data.name in bloc_syntenies ) && (!(d.data.name in genomesWC) || (chromo_only == 0))) {
              var contig = d.data.name in genomesWC;
              var bloc_synteny = bloc_syntenies[d.data.name];
              for (var j=0; j< bloc_synteny.length; j++  ) {
                  var locnode = {
                    x: xfam + j * min_blocksize,
                    y: yfam,
                    name: bloc_synteny[j][0], //bloc_name
                    val: bloc_synteny[j][1],  // bloc val
                    chromosome: bloc_synteny[j][2],  // bloc chromosome
                    col_chromo: bloc_synteny[j][3],  // bloc couleur chromosome
                    opa_chromo: bloc_synteny[j][4],  // bloc opacite chromosome
                    ctg: contig // booleen, si oui c'est un contifg si non c'est un chromo
                  }
                  genedataset.push(locnode);
              }
          }
      });

      // Gere les svg correpondant aux blocs de genes
        var geneblock =
        gtree.selectAll(".block")
        .data(genedataset);
        //EXIT
        geneblock.exit().remove();
        //ENTER
        var geneblockEnter =
        geneblock
        .enter()
        .append("rect")
        .attr("class", "block");
        //ENTER + updatesATE
        geneblockEnter
        .merge(geneblock)
        .attr("transform",function(d,i) {
          var transx = d.x + shift_text;
          var transy = d.y ;
          return "translate("+transx+", "+transy+")";
        })
        .attr("width", min_blocksize+"px")
        .attr("height", block_height+"px")
        .style("fill", function(d){
          var _col = myColor[colour_scale_index](d.val * 100);
          return _col
          })
        .on('mouseover',function (event, d){
          div.transition()
          .duration(200)
          .style("opacity", .9);
          div.html(function() {
            return "<b> " + d.name+" "+d.val+"</b> ";
          })
          .style("left", (event.pageX) + "px")
          .style("top", (event.pageY - 28) + "px")
        })
        .on('mouseout',function (d){
          div.transition()
          .duration(500)
          .style("opacity", 0);
        });
  /*    .on("click", function(event, d) {
          var _chromo = await chromo_gene(d.name);
          console.log("CHROMO "+_chromo);
          console.log(_chromo);
      });*/

        // Gere les svg correpondant aux blocs de genes
        var chromoblock =
          gtree.selectAll(".chromosome")
          .data(genedataset);
          //EXIT
          chromoblock.exit().remove();
          //ENTER
          var chromoblockEnter =
          chromoblock
          .enter()
          .append("rect")
          .attr("class", "block");
          //ENTER + updatesATE
          chromoblockEnter
          .merge(chromoblock)
          .attr("transform",function(d,i) {
            var transx = d.x + shift_text;
            var transy = d.y - block_height;
            return "translate("+transx+", "+transy+")";
          })
          .attr("width", min_blocksize+"px")
          .attr("height",chromo_block_height+"px")
          .style("fill", function(d){
            var _col = d.col_chromo;
            return _col
            })
          .style("opacity", function(d){
            return d.opa_chromo
            })
          .on('mouseover',function (event, d){
            div.transition()
            .duration(200)
            .style("opacity", .9);
            div.html(function() {
              if (d.ctg) {
                return "<b> Contig " + d.chromosome +"</b> ";
              } else {
                return "<b> Chromosome " + d.chromosome +"</b> ";
              }
            })
            .style("left", (event.pageX) + "px")
            .style("top", (event.pageY - 28) + "px")
          })
          .on('mouseout',function (d){
            div.transition()
            .duration(500)
            .style("opacity", 0);
          });

          // D3: Objets CARTE CHROMOSOME
          // ---------------------------
          var cartechromo =
          carteg.selectAll(".cartechromo")
          .data(chromo_carte);
          //EXIT
          cartechromo.exit().remove();
          //ENTER
          var cartechromoEnter =
          cartechromo
          .enter()
          .append("rect")
          .attr("class", "cartechromo");
          //ENTER + UPDATE
          cartechromoEnter
          .merge(cartechromo)
          .attr("transform",function(d,i) {
            var transx = d.first / scale_carte ;
            var transy = 8 ;
            return "translate("+transx+", "+transy+")";
          })
          .attr("width", function(d,i) {
            var w = (d.last - d.first) / scale_carte +"px";
            return w;
          })
          .attr("height", "50px")
          .attr("stroke", "black")
          .on('mouseover',function (event,d){
            console.log(d)
            console.log(d.name)
            if (d.name) {
              if (d.name != "UNKNOWN") {
                divcarte.transition()
                .duration(100)
                .style("opacity", .9);
                divcarte.html(function() {
                  if (referenceGenome in genomesWC) {
                    return "<b> contig "+  d.name + "</b> ";
                  } else {
                    return "<b> chromosome "+  d.name + "</b> ";
                  }
                })
                .style("left", (event.pageX) + "px")
                .style("top", "150px")
                };
            };
          })
          .on('mouseout',function (d){
            divcarte.transition()
            .duration(50)
            .style("opacity", 0);
          })
          .style("fill", function(d,i){ return d.col });
          d3.select("#densite").text("Genes per block : "+ zoomLevel + ", max number of blocks :"+ max_nb_of_blocks);
          d3.select("#range-label").text("Genes  "+ firstGene +" to " + lastGene);

  }


  // Fonction qui  ouvre les noeuds
  // ------------------------------
  function expandTree(tree) {
    tree.each(function (d) {
      if (d.data.nodeinfo) {
        if (d.data.nodeinfo.status === "collapsed") {
          if (d.data._clade) {
            d.data.clade = d.data._clade;
            d.data._clade = null;
            d.data.nodeinfo = {status : "extended"};
          } else {
            console.log("ERREUR");
          }
        }
      }
    });
  }


  // Fonction de mise a jour des donnees apres un changement
  // de genome de reference
  // -------------------------------------------------------
  function updateData(){
    console.log("UPDATE DATA START");
    d3.select("#loading").text("Updating data, please wait...");
    // Recupere le tableau de genes du genome de reference
    var genes_carte = relatives_syntenies[referenceGenome];
    if (genes_carte !== undefined) {
      //  recalcule la carte du chromosome
      chromo_carte = [];
      var curr_chromo = "UNDEFINED";
      var cur_col = "pink";
      var ichrom = 1;
      var first = 0;
      var last = "NONE" ;
      genes_carte.forEach (function (d,i){
        var _chromo = d[2];
        var _gene =  d[0];
        var _colchro =  d[3];
        if (_gene != "NO GENE") {
        if (_chromo !== curr_chromo) {
          if (curr_chromo != "UNDEFINED") {
            last = ichrom;
            var chromonode = {name:curr_chromo,first: first,last:last,col:cur_col};
            chromo_carte.push(chromonode);
          }
        curr_chromo = _chromo;
        cur_col =  _colchro;
        first = ichrom;
      }}
        ichrom = ichrom + 1;
      })
      last = ichrom;
      var chromonode = {name:curr_chromo,first: first,last:last,col:cur_col};
      chromo_carte.push(chromonode);
    }
    console.log(chromo_carte);
    console.log("UPDATE DATA END");
  }
  // Fonction d'export du SVG
  // ------------------------
  function saveSVG(){
    // get styles from all required stylesheets
    // http://www.coffeegnome.net/converting-svg-to-png-with-canvg/
    var style = "\n";
    // .label {
    //         font: 8px sans-serif;
    //       }
    var requiredSheets = ['phylogram_d3.css', 'open_sans.css'];
    for (var i=0; i<document.styleSheets.length; i++) {
      console.log(i)
          var sheet = document.styleSheets[i];
          console.log(sheet)
          if (sheet.href) {
              var sheetName = sheet.href.split('/').pop();
              if (requiredSheets.indexOf(sheetName) != -1) {
                  var rules = sheet.rules;
                  if (rules) {
                      for (var j=0; j<rules.length; j++) {
                          style += (rules[j].cssText + '\n');
                      }
                  }
              }
          }
      }
    var img = new Image();
    var serializer = new XMLSerializer();
    // prepend style to svg
    // document.style.color = 'red';
    svg.insert('defs',":first-child")
    d3.select("svg defs")
        .append('style')
        .attr('type','text/css')
        .html("font: 8px sans-serif");
    var svgStr = serializer.serializeToString(svg.node());
    img.src = 'data:image/svg+xml;base64,'+window.btoa(unescape(encodeURIComponent(svgStr)));
    window.open().document.write('<p>Please copy or save the the image <img src="' + img.src + '"/>');
  };

  // BOUTONS
  $('#action').on('click', function(event, dropdownData) {
    var target = event.toElement || event.relatedTarget || event.target || function () { throw "Failed to attach an event target!"; }
    action_description = target.text;
    switch (target.text) {
      case "Switch children":
        action = svgEvents.switchChildren;
        $('#switch').css('color','red');
        $('#collapse').css('color','black');
        $('#subtree').css('color','black');
        $('#reference').css('color','black');
        break;
      case "Collapse/Expand":
        action = svgEvents.collapse;
        $('#switch').css('color','black');
        $('#collapse').css('color','red');
        $('#subtree').css('color','black');
        $('#reference').css('color','black');
        // updateLayout(cladeRoot,0,1);
        break;
      case "Sub Tree/Upper Tree":
        action = svgEvents.focus;
        $('#switch').css('color','black');
        $('#collapse').css('color','black');
        $('#subtree').css('color','red');
        $('#reference').css('color','black');
        // updateLayout(cladeRoot,0,1);
        break;
      case "Choose as reference":
        action = svgEvents.reference;
        $('#switch').css('color','black');
        $('#collapse').css('color','black');
        $('#subtree').css('color','black');
        $('#reference').css('color','red');
        // updateLayout(cladeRoot,0,1);
        break;
      default:
    }
  });

  $('#zoomlevel').on('click', function(event, dropdownData) {
    var target = event.toElement || event.relatedTarget || event.target || function () { throw "Failed to attach an event target!"; }
    var _zoomLevel = zoomLevel;
    switch (target.text) {
      case "250 genes/bloc":
        zoomLevel = 250;
        fixed  = 1;
        $('#zoomlevel1').css('color','red');
        $('#zoomlevel2').css('color','black');
        $('#zoomlevel3').css('color','black');
        $('#zoomlevel4').css('color','black');
        $('#zoomlevel5').css('color','black');
        d3.selectAll("#minuse1").attr("opacity",1.0);
        d3.selectAll("#maxuse2").attr("opacity",1.0);
        d3.selectAll("#maxuse2").attr("x", Math.min(firstGene + maxEcart, genome_length[referenceGenome]) / scale_carte);
        d3.selectAll("#minuse1").attr("x", Math.max(0,lastGene - maxEcart) / scale_carte);
        $('#fixed').css('opacity',0.3);
        break;
      case "100 genes/bloc":
        zoomLevel = 100;
        fixed  = 1;
        $('#zoomlevel1').css('color','black');
        $('#zoomlevel2').css('color','red');
        $('#zoomlevel3').css('color','black');
        $('#zoomlevel4').css('color','black');
        $('#zoomlevel5').css('color','black');
        d3.selectAll("#minuse1").attr("opacity",1.0);
        d3.selectAll("#maxuse2").attr("opacity",1.0);
        d3.selectAll("#maxuse2").attr("x", Math.min(firstGene + maxEcart, genome_length[referenceGenome]) / scale_carte);
        d3.selectAll("#minuse1").attr("x", Math.max(0,lastGene - maxEcart) / scale_carte);
        $('#fixed').css('opacity',0.3);
        break;
      case "50 genes/bloc":
        zoomLevel = 50;
        fixed  = -1;
        $('#zoomlevel1').css('color','black');
        $('#zoomlevel2').css('color','black');
        $('#zoomlevel3').css('color','red');
        $('#zoomlevel4').css('color','black');
        $('#zoomlevel5').css('color','black');
        d3.selectAll("#minuse1").attr("opacity",0.0);
        d3.selectAll("#maxuse2").attr("opacity",0.0);
        d3.selectAll("#maxuse2").attr("x", (firstGene + maxEcart) / scale_carte);
        d3.selectAll("#minuse1").attr("x", (lastGene - maxEcart) / scale_carte);
        $('#fixed').css('opacity',1.0);
        break;
      case "10 genes/bloc":
        zoomLevel = 10;
        fixed  = -1;
        $('#zoomlevel1').css('color','black');
        $('#zoomlevel2').css('color','black');
        $('#zoomlevel3').css('color','black');
        $('#zoomlevel4').css('color','red');
        $('#zoomlevel5').css('color','black');
        d3.selectAll("#minuse1").attr("opacity",0.0);
        d3.selectAll("#maxuse2").attr("opacity",0.0);
        d3.selectAll("#maxuse2").attr("x", (firstGene + maxEcart) / scale_carte);
        d3.selectAll("#minuse1").attr("x", (lastGene - maxEcart) / scale_carte);
        $('#fixed').css('opacity',1.0);
        break;
      case "1 gene/bloc":
        zoomLevel = 1;
        fixed  = -1;
        $('#zoomlevel1').css('color','black');
        $('#zoomlevel2').css('color','black');
        $('#zoomlevel3').css('color','black');
        $('#zoomlevel4').css('color','black');
        $('#zoomlevel5').css('color','red');
        d3.selectAll("#minuse1").attr("opacity",0.0);
        d3.selectAll("#maxuse2").attr("opacity",0.0);
        $('#fixed').css('opacity',1.0);
        break;
      default:
      };
      if (zoomLevel != _zoomLevel) {
      var nb_blocs = (lastGene - firstGene) / zoomLevel;
      lastGene = Math.min(firstGene + max_nb_of_blocks * zoomLevel, genome_length[referenceGenome]);
      var _diff =  firstGene + max_nb_of_blocks * zoomLevel - lastGene;
      if  (_diff > 0) {
          firstGene = Math.max(1,firstGene - _diff);
          d3.selectAll("#use1").attr("x", firstGene / scale_carte);
      }
      maxEcart = lastGene - firstGene + 1 ;
      // deplace le pointeur droit
      d3.selectAll("#use2").attr("x", lastGene / scale_carte);
      // d3.selectAll("#minuse1").attr("x", limitFirstGene / scale_carte);
      d3.selectAll("#maxuse2").attr("x", lastGene / scale_carte);
      d3.selectAll("#maxuse2").attr("x", (firstGene + maxEcart) / scale_carte);
      d3.selectAll("#minuse1").attr("x", (lastGene - maxEcart) / scale_carte);

      bloc_syntenies = calculate_bloc();
      updateLayout();
      }
    });
    $('#whole').on('click', function(event, dropdownData) {
      lastGene =  genome_length[referenceGenome];
      firstGene = 1;
      maxEcart = lastGene - firstGene + 1;
      fixed = 1;
      cladeRoot = globalRoot;
      var treeRoot = d3.hierarchy(cladeRoot, function(d) {
        return d.clade;
      });
      expandTree(treeRoot);
      // var nb_leaves = total_nb_leaves;
      // max_nb_of_blocks = Math.round(max_nb_of_blocks_total / (nb_leaves + 1 ));
      zoomLevel = Math.floor(lastGene / max_nb_of_blocks);
      $('#zoomlevel1').css('color','black');
      $('#zoomlevel2').css('color','black');
      $('#zoomlevel3').css('color','black');
      $('#zoomlevel4').css('color','black');
      $('#zoomlevel5').css('color','black');
      d3.selectAll("#use1").attr("x", firstGene / scale_carte);
      d3.selectAll("#use2").attr("x", lastGene / scale_carte);
      d3.selectAll("#minuse1").attr("opacity",1.0);
      d3.selectAll("#maxuse2").attr("opacity",1.0);
      d3.selectAll("#maxuse2").attr("x", Math.min(firstGene + maxEcart, genome_length[referenceGenome]) / scale_carte);
      d3.selectAll("#minuse1").attr("x", Math.max(0,lastGene - maxEcart) / scale_carte);
      $('#fixed').css('opacity',0.3);
      bloc_syntenies = calculate_bloc();
      nodeWidth = 5000;       // Largeur du noeud
      nodeHeight =20;      // Hauteur du noeud
      block_height = 4;
      chromo_block_height = block_height;    // Hauteur en pixel d'un bloc de genes
      display_chromo = 1;
      $('#chromo').css('opacity',1.0);
      updateLayout();
      window.scrollTo(0, 0);
    });
    $('#moreHeigth').on('click', function(event, dropdownData) {
      nodeHeight = nodeHeight + step;
      if (nodeHeight > minNodeHeight) {
        if ( display_chromo == 1 ) {
          block_height = nodeHeight /2 ;
          chromo_block_height = block_height;
        } else {
          block_height = nodeHeight * 0.9 ;
          chromo_block_height = 0;
        }
      }
      updateLayout();
    });
    $('#lessHeigth').on('click', function(event, dropdownData) {
      if ((nodeHeight - 2)  >= minNodeHeight) {
        nodeHeight = nodeHeight - 2;
        if ( display_chromo == 1 ) {
          block_height = nodeHeight /2 ;
          chromo_block_height = block_height;
        } else {
          block_height = nodeHeight * 0.9 ;
          chromo_block_height = 0;
        }
        // block_height = nodeHeight /2 ;
        // chromo_block_height = block_height;
        // if (nodeHeight <= minNodeHeight) {
        //   chromo_block_height = 0;
        //   block_height = nodeHeight * 0.9 ;
        // }
        updateLayout();
      }
    });
    $('#moreTreeWidth').on('click', function(event, dropdownData) {
      console.log(nodeWidth)
      nodeWidth = nodeWidth + step;
      updateLayout();
    });
    $('#lessTreeWidth').on('click', function(event, dropdownData) {
      nodeWidth = nodeWidth - step;
      updateLayout();
    });

    $('#assemblies').on('click', function(event, dropdownData) {
      if ( chromo_only == 0 ) {
        chromo_only = 1;
        $('#assemblies').css('opacity',0.3);
      } else {
        chromo_only = 0;
        $('#assemblies').css('opacity',1.0);
      }
      updateLayout();
    });

    $('#fixed').on('click', function(event, dropdownData) {
      fixed = - fixed;
      if (fixed == -1) { //fixe
        $('#fixed').css('opacity',1.0);
        d3.selectAll("#minuse1").attr("opacity",0);
        d3.selectAll("#maxuse2").attr("opacity",0);
      } else { //libre
        d3.selectAll("#minuse1").attr("opacity",1.0);
        d3.selectAll("#maxuse2").attr("opacity",1.0);
        d3.selectAll("#maxuse2").attr("x", (firstGene + maxEcart) / scale_carte);
        d3.selectAll("#minuse1").attr("x", (lastGene - maxEcart) / scale_carte);
        $('#fixed').css('opacity',0.3);
      }
    });

    $('#chromo').on('click', function(event, dropdownData) {
      if ( display_chromo == 1 ) {
        display_chromo = 0.0 ;
        block_height = nodeHeight * 0.9 ;
        chromo_block_height = 0.0 ;
        $('#chromo').css('opacity',0.3);
      } else {
        display_chromo = 1 ;
        block_height = nodeHeight / 2.0 ;
        chromo_block_height = block_height;
        $('#chromo').css('opacity',1.0);
      }
      updateLayout();
    });

    $('#display a').on( 'click', function( event ) {
      var $target = $( event.currentTarget ),
      val = $target.attr( 'data-value' ),
      $inp = $target.find( 'input' ),
      idx;
      if (val == "intname") {
        if  ( disp_intnode == 1 ) {
          disp_intnode = 0;
          setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
        }
        else {
          disp_intnode = 1;
          setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
        }
      } else {
        disp_leaves = val;
        $('#code').css('color','black');
        $('#short').css('color','black');
        $('#long').css('color','black');
        switch ( val ) {
          case "code" :
            shift_text = 50;
            $('#code').css('color','red');
            break;
          case "short" :
            shift_text = 100;
            $('#short').css('color','red');
            break;
          case "long" :
            shift_text = 200;
            $('#long').css('color','red');
            break;
          default:
        }
    }
      // $( event.target ).blur();
      // myStorage.setItem("options",options);

      console.log("disp_intnode = "+disp_intnode+", disp_leaves = "+val);
      console.log($inp);
      updateLayout(cladeRoot);
      return false;
    });


    $('#colourscale').on('click', function(event, dropdownData) {
      var target = event.toElement || event.relatedTarget || event.target || function () { throw "Failed to attach an event target!"; }
      // myStorage.setItem("colourscale",target.text);
      console.log("colourscale= "+target.text);
      switch (target.text) {
        case "White to Blue":
          colour_scale_index = 0;
          $('#colourscale1').css('color','red');
          $('#colourscale2').css('color','black');
          $('#colourscale3').css('color','black');
          $('#colourscale4').css('color','black');
          updateLayout(cladeRoot);
          // main_display();
          break;
        case "Blue to White":
          colour_scale_index = 1;
          $('#colourscale1').css('color','black');
          $('#colourscale2').css('color','red');
          $('#colourscale3').css('color','black');
          $('#colourscale4').css('color','black');
          updateLayout(cladeRoot);
          // main_display();
          break;
        case "Green to Red":
          colour_scale_index = 2;
          $('#colourscale1').css('color','black');
          $('#colourscale2').css('color','black');
          $('#colourscale3').css('color','red');
          $('#colourscale4').css('color','black');
          // main_display();
          updateLayout(cladeRoot);
          break;
        case "Red to Green":
          colour_scale_index = 3;
          $('#colourscale1').css('color','black');
          $('#colourscale2').css('color','black');
          $('#colourscale3').css('color','black');
          $('#colourscale4').css('color','red');
          updateLayout(cladeRoot);
          // main_display();
          break;
        default:
      }
    });
    $('#export').on('click', function() {
      saveSVG();
    });

    drawTree();
}
  

